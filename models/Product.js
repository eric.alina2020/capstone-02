// DEPENDENCIES AND MODULES
	const mongoose = require("mongoose");

// SCHEMA / BLUEPRINT
	 const productSchema = new mongoose.Schema({
	 	name: {
	 		type: String,
	 		required: [true, 'Product Name is REQUIRED.']
	 	},
	 	description: {
	 		type: String,
	 		required: [true, 'Product Name is REQUIRED.']
	 	},
	 	price: {
	 		type: Number,
	 		required: [true, 'Product Price is REQUIRED.']
	 	},
	 	isActive: {
	 		type: Boolean,
	 		default: true
	 	},
	 	stocks: {
	 		type: Number,
	 		required: [true, "Number of stocks is REQUIRED."]
	 	},
	 	createdOn: {
	 		type: Date,
	 		default: new Date()
	 	} 
	 });

// MODEL
	const Product = mongoose.model("Product", productSchema);
	module.exports = Product;
	