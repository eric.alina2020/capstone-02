// DEPENDENCIES AND MODULES
	const mongoose = require("mongoose");

// SCHEMA / BLUEPRINT
	const orderSchema = new mongoose.Schema({
		totalAmount: {
			type: Number,
			required: [true, "Total Amount is REQUIRED."]
		},
		orderedBy: {
			type: String,
			required: [true, "User email is REQUIRED."]
		},
		processedOn: {
			type: Date,
			default: new Date()
		},
		productName: {
			type: String,
			required: [true, "Product Name is REQUIRED."]
		},
		price: {
			type: Number,
			required: [true, "Product Price is REQUIRED."]
		},
		quantity: {
			type: Number,
			required: [true, "Order Quantity is REQUIRED."]
		}
	});

// MODEL
	const Order = mongoose.model('Order', orderSchema);
	module.exports = Order;
	