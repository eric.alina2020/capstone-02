// DEPENDENCIES AND MODULES
	const mongoose = require("mongoose");

// SCHEMA / BLUEPRINT
	const userSchema = new mongoose.Schema({
		email: {
			type: String,
			required: [true, 'Your email is REQUIRED.']
		},
		password: {
			type: String,
			required: [true, 'Your password is REQUIRED.']
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		isActive: {
			type: Boolean,
			default: true
		},
		ordersHistory: [
			{
				productName: {
					type: String,
					required: [true, "Product Name is REQUIRED."]
				},
				price: {
					type: Number,
					required: [true, "Product Price is REQUIRED."]
				},
				quantity: {
					type: Number,
					required: [true, "Product Quantity is REQUIRED."]
				},
				processedOn: {
					type: Date,
					default: new Date()
				}
			}
		],
		checkoutHistory: [
			{
				totalAmount: {
					type: Number,
					required: [true, "Total Amount is REQUIRED."]
				},
				productName: {
					type: String,
					required: [true, "Product Name is REQUIRED."]
				},
				quantity: {
					type: Number,
					required: [true, 'Product Quantity is REQUIRED.']
				},
				purchasedOn: {
					type: Date,
					default: new Date()
				},				
				paymentMethod: {
					type: String,
					required: [true, "Payment Method is REQUIRED."]
				},
				shippingAddress: {
					type: String,
					required: [true, "Shipping Address is REQUIRED."]
				}
			}
		]
	});

// MODEL
	const User = mongoose.model('User', userSchema);
	module.exports = User;
	