// IMPORTS / DEPENDENCIES / MODULES
	const jwt = require("jsonwebtoken");
	const secret = "Capstone02eCommerceAPI"

// TOKEN CREATION
	module.exports.createAccessToken = (user) => {
		const data = {
			id: user._id,
			email: user.email,
			password: user.password,
			isAdmin: user.isAdmin
		};
		return jwt.sign(data, secret, {});
	};

// TOKEN VERIFICATION
	module.exports.verify = (req, res, next) => {
		let token = req.headers.authorization;

		if (typeof token === "undefined") {
			return res.send({auth: "Failed. No Token"});
		} else {
			token = token.slice(7, token.length);

			jwt.verify(token, secret, (err, decodedToken) => {
				if (err) {
					return res.send({
						authentication: "Failed",
						message: err.message
					});
				} else {
					req.user = decodedToken;
					next();
				};
			});
		};
	};

	module.exports.verifyAdmin = (req, res, next) => {
		if (req.user.isAdmin) {
			next();
		} else {
			return res.send({
				authentication: "Failed",
				message: "Action Forbidden."
			});
		};
	};