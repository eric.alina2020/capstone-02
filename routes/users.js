// DEPENDENCIES AND MODULES
	const exp = require("express");
	const controller = require("../controllers/users");
	const auth = require("../auth");

	const {verify, verifyAdmin} = auth;

// ROUTING COMPONENTS
	const route = exp.Router();

// [POST] ROUTES

	// User Signup (User)
	route.post("/signup", controller.signUp);

	// User Login (User)
	route.post("/login", controller.login);

// [GET] ROUTES

	// Retrieves A Single User (User & Admin)
	route.get("/userInfo", verify, controller.getSingleUser);

	// Retrieves All Users (Admin)
	route.get("/all", verify, verifyAdmin, controller.getAllUsers);

	// Retrieves Active Users (Admin)
	route.get("/active", verify, verifyAdmin, controller.getActiveUsers);

// [PUT] ROUTES

	// Updates User Password (User)
	route.put("/update-password", verify, controller.updatePassword);

	// Updates User As Admin (Admin)
	route.put("/updateUser-as-admin", verify, verifyAdmin, controller.updateUserAsAdmin);

	// Updates Admin As User (Admin)
	route.put("/updateAdmin-as-user", verify, verifyAdmin, controller.updateAdminAsUser);

	// Deactivates User (User & Admin)
	route.put("/deactivate-user", verify, controller.deactivateUser);

	// Activates User (User & Admin)
	route.put("/activate-user", verify, controller.activateUser);

// [DELETE] ROUTES

	// Deletes User (User & Admin)
	route.delete("/delete-user", verify, controller.deleteUser);

	// Deletes Admin (Admin)
	route.delete("/delete-admin", verify, verifyAdmin, controller.deleteAdmin);

// EXPOSE ROUTE SYSTEM
	module.exports = route;
	