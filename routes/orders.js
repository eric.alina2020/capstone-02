// DEPENDENCIES AND MODULES
	const exp = require("express");
	const controller = require("../controllers/orders");
	const auth = require("../auth");

	const {verify, verifyAdmin} = auth;

// ROUTING COMPONENT
	const route = exp.Router();

// [POST] ROUTES

	// Creates An Order (User)
	route.post("/create", verify, controller.createOrder);

	// Checkout Order (User & Admin)
	route.post("/checkout", verify, controller.checkoutOrder);

// [GET] ROUTES

	// Retrieves All Orders (Admin)
	route.get("/all", verify, verifyAdmin, controller.retrieveAllOrders);

	// Retrieves A Single Order (User & Admin)
	route.get("/orderInfo", verify, controller.getSingleOrder);


// [PUT] ROUTES

// [DELETE] ROUTES

// EXPORT ROUTE SYSTEM
	module.exports = route;