// DEPENDENCIES AND MODULES
	const exp = require("express");
	const controller = require("../controllers/products");
	const auth = require("../auth");

	const {verify, verifyAdmin} = auth;

// ROUTING COMPONENT
	const route = exp.Router();

// [POST] ROUTES

	// Creates Product (Admin)
	route.post("/create", verify, verifyAdmin, controller.createProduct);

// [GET] ROUTES

	// Retrieves All Active Products (User & Admin)
	route.get("/active", verify, controller.getActiveProducts);

	// Retrieves A Single Product (User & Admin)
	route.get("/productInfo", verify, controller.getSingleProduct);

	// Retrieves All Products (User & Admin)
	route.get("/all", verify, controller.getAllProducts);

// [PUT] ROUTES

	// Updates Product Price (Admin)
	route.put("/update-price", verify, verifyAdmin, controller.updateProductPrice);

	// Updates Product Inventory (Admin)
	route.put("/update-stocks", verify, verifyAdmin, controller.updateStocks);

	// Archives A Product (Admin)
	route.put("/archive-product", verify, verifyAdmin, controller.archiveProduct);

	// Activates A Product (Admin)
	route.put("/activate-product", verify, verifyAdmin, controller.activateProduct);

// [DELETE] ROUTES
	
	// Deletes A Product (Admin)
	route.delete("/delete-product", verify, verifyAdmin, controller.deleteProduct);

// EXPORT ROUTE SYSTEM
	module.exports = route;