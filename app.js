// PACKAGES AND DEPENDENCIES
	const express = require("express");
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const userRoutes = require("./routes/users");
	const productRoutes = require("./routes/products");
	const orderRoutes = require("./routes/orders");

// SERVER SETUP
	const app = express();
	dotenv.config();
	app.use(express.json());
	const db_secret = process.env.CONNECTION_STRING;
	const port = process.env.PORT;

// APPLICATION ROUTES
	app.use("/users", userRoutes);
	app.use("/products", productRoutes);
	app.use("/orders", orderRoutes);

// DATABASE CONNECTION
	mongoose.connect(db_secret);
	let connectStatus = mongoose.connection;
	connectStatus.on("open", () => console.log('Database is Connected'));

// GATEWAY RESPONSE
	app.get("/", (req, res) => {
		res.send("Welcome to Deluxe, the No. 1 Telecommunications Company in the Planet. Experience fast internet speeds with the latest phone technologies brought to you by Apple, Google, and Samsung.");
	});

	app.listen(port, () => console.log(`Server is running on port ${port}`));