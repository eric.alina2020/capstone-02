// DEPENDENCIES AND MODULES
	const Product = require("../models/Product");

// [CREATE] FUNCTIONALITIES

	// Creates Product (Admin)
	module.exports.createProduct = (req, res) => {
		let prodName = req.body.name;
		let prodDesc = req.body.description;
		let prodPrice = req.body.price;
		let stocks = req.body.stocks;

		// finds a product by its name if currently existing or not.
		Product.findOne({name: prodName})
		.then(foundProduct => {
			if (foundProduct !== null) {
				return res.send(`Product information are already created.`);
			} else {

				// conditional statement for the required fields to be filled out.
				if (prodName !== "" && prodDesc !== "" && prodPrice !== "" && stocks !== "") {

					// creates a new instance of a product.
					let newProduct = new Product({
						name: prodName,
						description: prodDesc,
						price: prodPrice,
						stocks: stocks
					});

					// saves the new product.
					return newProduct.save()
						.then(savedProduct => {
							if (savedProduct) {
								return res.send(savedProduct);
							} else {
								return res.send(`Failed to save a new product: ${prodName}. Kindly check all the details.`);
							};
						})
						.catch(err => {
							return res.send(err.message);
						});
				} else {
					return res.send("Please enter all the required information.");
				};
			};
		})
		.catch(err => {
			return res.send(err.message);
		});
	};

// [RETRIEVE] FUNCTIONALITIES

	// Retrieves All Active Products (User & Admin)
	module.exports.getActiveProducts = (req, res) => {

		// finds a product by its active status.
		Product.find({isActive: true})
		.then(foundProducts => {
			return res.send(foundProducts);
		})
		.catch(err => {
			return res.send(err.message);
		});
	};

	// Retrieves A Single Product (User & Admin)
	module.exports.getSingleProduct = (req, res) => {
		let id = req.body.productId;

		// conditional statement for the required fields to be filled out.
		if (id !== "") {

			// finds a product using its id.
			Product.findById({_id: id})
			.then(foundProduct => {
				if (foundProduct !== null) {
					return res.send(foundProduct);
				} else {
					return res.send("Product information cannot be found. Make sure you provided the correct product Id.");
				};
			})
			.catch(err => {
				res.send("Incorrect product Id. Try again.");
			});
		} else {
			return res.send("Please enter the required information.");
		};
	};

	// Retrieves All Products (User & Admin)
	module.exports.getAllProducts = (req, res) => {
		Product.find({})
		.then(foundProducts => {
			return res.send(foundProducts);
		})
		.catch(err => {
			return res.send(err.message);
		});
	};

// [UPDATE] FUNCTIONALITIES
	
	// Updates Product Price (Admin)
	module.exports.updateProductPrice = (req, res) => {
		let id = req.body.productId;
		let price = req.body.newPrice;

		// conditional statement for the required fields to be filled out.
		if (id !== "" && price !== null) {

			// finds a product by its id and updates its price.
			Product.findByIdAndUpdate({_id: id}, {price: price}, {new: true})
			.then(foundProduct => {
				if (foundProduct !== null) {
					return res.send(`Updated Product Price Details: ${JSON.stringify(foundProduct)}`);
				} else {
					return res.send("Product information cannot be found. Make sure you provided the correct product Id and its corresponding new price.");
				};
			})
			.catch(err => {
				return res.send("Incorrect product Id and/or bad input for price. Try again.");
			})			
		} else {
			return res.send("Please enter all the required information.");
		};
	};

	// Updates Product's Stocks (Admin)
	module.exports.updateStocks = (req, res) => {
		let name = req.body.name;
		let stocks = req.body.stocks;

		// conditional statement for the required fields to be filled out.
		if (name !== "" && stocks !== "") {

			// finds a product by its name and update its number of available stocks.
			Product.findOneAndUpdate({name: name}, {stocks: stocks}, {new: true})
			.then(updatedProduct => {
				if (updatedProduct !== null) {
					return res.send(`Updated Product Stock Details: ${JSON.stringify(updatedProduct)}`);					
				} else {
					return res.send("Product information cannot be found. Make sure you provided the correct name and the intended number of stocks.");
				};
			})
			.catch(err => {
				return res.send("Incorrect product name and/or bad input for stocks. Try again.");
			});
		} else {
			return res.send("Please enter all the required information.");
		};
	};

	// Archives A Product (Admin)
	module.exports.archiveProduct = (req, res) => {
		let id = req.body.productId;

		// conditional statement for the required fields to be filled out.
		if (id !== "") {

			// finds a product by its id and deactivates/archives.
			Product.findByIdAndUpdate({_id: id}, {isActive: false}, {new: true})
			.then(archivedProduct => {
				if (archivedProduct !== null) {
					return res.send(`Archived Product Details: ${JSON.stringify(archivedProduct)}`);
				} else {
					return res.send("Product information cannot be found. Make sure you provided the correct product Id.");
				};
			})
			.catch(err => {
				return res.send("Incorrect product Id. Try again.");
			})			
		} else {
			return res.send("Please enter the required information.");
		};
	};

	// Activates A Product (Admin)
	module.exports.activateProduct = (req, res) => {
		let id = req.body.productId;

		// conditional statement for the required fields to be filled out.
		if (id !== "") {

			// finds a product by its id and activates.
			Product.findByIdAndUpdate({_id: id}, {isActive: true}, {new: true})
			.then(activatedProduct => {
				if (activatedProduct !== null) {
					return res.send(`Activated Product Details: ${JSON.stringify(activatedProduct)}`);
				} else {
					return res.send("Product information cannot be found. Make sure you provided the correct product Id.");
				};
			})
			.catch(err => {
				return res.send("Incorrect product Id. Try again.");
			})			
		} else {
			return res.send("Please enter the required information.");
		};
	};

// [DELETE] FUNCTIONALITIES

	// Deletes A Product (Admin)
	module.exports.deleteProduct = (req, res) => {
		let id = req.body.productId;

		// conditional statement for the required fields to be filled out.
		if (id !== "") {

			// finds a product by its id and deletes.
			Product.findByIdAndDelete({_id: id})
			.then(deletedProduct => {
				if (deletedProduct !== null) {
					return res.send(`Deleted Product Details: ${JSON.stringify(deletedProduct)}`);
				} else {
					return res.send("Product information cannot be found.");
				};				
			})
			.catch(err => {
				return res.send("Incorrect product Id. Try again.");
			});
		} else {
			return res.send("Enter the required information.");
		};
	};