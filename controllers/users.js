// DEPENDENCIES AND MODULES
	const User = require("../models/User");
	const Product = require("../models/Product");
	const bcrypt = require("bcrypt");
	const dotenv = require("dotenv");
	const auth = require("../auth");
	

// ENVIRONMENT SETUP
	dotenv.config();
	const salt = Number(process.env.SALT);

// [CREATE] FUNCTIONALITIES

	// User Signup (User)
	module.exports.signUp = (req, res) => {
		let email = req.body.email;
		let password = req.body.password;

		// finds a user by its email if already existing or not.
		User.findOne({email: email})
		.then(existingUser => {
			if (existingUser !== null && existingUser.email == email) {
				return res.send(`Duplicate accounts found using the same ${email}. Try using a different email.`);
			} else {

				// conditional statement for the required fields to be filled out.
				if (email !== "" && password !== "") {

					// creates a new instance of a user
					let newUser = new User({
						email: email,
						password: bcrypt.hashSync(password, salt)
					});

					// saves new user.
					return newUser.save()
						.then(savedUser => {
							return res.send(savedUser);
						})
						.catch(err => {
							return res.send("Registration failed. Try again.");
						});
				} else {
					return res.send("Please enter all the required information.");
				};
			};
		})
		.catch(err => {
			return res.send(err.message);
		});
	};

	// User Login (User)
	module.exports.login = (req, res) => {
		let email = req.body.email;
		let password = req.body.password;

		// conditional statement for the required fields to be filled out.
		if (email !== "" && password !== "") {

			// finds a user by its email if already existing or not.
			User.findOne({email: email})
			.then(foundUser => {
				if (foundUser === null) {
					return res.send(`User is not found. Make sure that you signed up before logging in and your email ${email} is currently associated to your account.`);
				} else {

					// returns a Boolean if the hashed user password matches to the given password.
					const isPasswordCorrect = bcrypt.compareSync(password, foundUser.password);

					// if the condition is true, it generates an access token for the user.
					if (isPasswordCorrect) {
						return res.send({accessToken: auth.createAccessToken(foundUser)});
					} else {
						return res.send("Incorrect password.");
					};
				};
			})
			.catch(err => {
				return res.send(err.message);
			});
		} else {
			return res.send("Please enter all the required information.");
		};
	};

// [RETRIEVE] FUNCTIONALITIES
	
	// Retrieves A Single User (User & Admin)
	module.exports.getSingleUser = (req, res) => {
		let id = req.body.userId;

		// conditional statement for the required fields to be filled out.
		if (id !== "") {

			// finds a user by its id.
			User.findById({_id: id})
			.then(foundUser => {
				if (foundUser !== null) {
					return res.send(foundUser);			
				} else {
					return res.send("User not found. Make sure you provided the correct user Id.")
				};
			})
			.catch(err => {
				return res.send("Incorrect user Id. Try again.");
			});
		} else {
			return res.send("Please enter the required the information.")
		};
	};

	// Retrieves All Users (Admin)
	module.exports.getAllUsers = (req, res) => {
		User.find({})
		.then(foundUsers => {
			return res.send(foundUsers);
		})
		.catch(err => {
			return res.send(err.message);
		});
	};

	// Retrieves Active Users (Admin)
	module.exports.getActiveUsers = (req, res) => {
		// finds a user by its active status.
		User.find({isActive: true})
		.then(activeUsers => {
			return res.send(activeUsers);
		})
		.catch(err => {
			return res.send(err.message);
		});
	};

// [UPDATE] FUNCTIONALITIES

	// Updates User Password (User)
	module.exports.updatePassword = (req, res) => {
		let email = req.body.email;
		let password = req.body.password;

		// conditional statement for the required fields to be filled out.
		if (email !== "" && password !== "") {

			// finds a user by its email and updates its password.
			User.findOneAndUpdate({email: email}, {password: bcrypt.hashSync(password, salt)}, {new: true})
			.then(updatedUserInfo => {
				if (updatedUserInfo !== null) {
					return res.send(`Password was updated: ${JSON.stringify(updatedUserInfo)}`);
				} else {
					return res.send(`Changing of password failed.`);
				};
			})
			.catch(err => {
				return res.send(err.message);
			});
		} else {
			return res.send("Please enter all the required information.");
		};
	};

	// Updates User As Admin (Admin)
	module.exports.updateUserAsAdmin = (req, res) => {
		let email = req.body.email;

		// conditional statement for the required fields to be filled out.
		if (email !== "") {

			// finds a user by its email and updates its role as admin.
			User.findOneAndUpdate({email: email}, {isAdmin: true}, {new: true})
			.then(updatedRole => {
				if (updatedRole !== null) {
					return res.send(`${email} authorization level was changed to Admin: ${JSON.stringify(updatedRole)}`);
				} else {
					return res.send(`User information cannot be found. Try again.`);
				};
			})
			.catch(err => {
				return res.send(err.message);
			});
		} else {
			return res.send("Please enter all the required information.");
		};
	};

	// Updates Admin As User (Admin)
	module.exports.updateAdminAsUser = (req, res) => {
		let email = req.body.email;

		// conditional statement for the required fields to be filled out.
		if (email !== "") {

			// finds an admin by its email and updates its role as user.
			User.findOneAndUpdate({email: email}, {isAdmin: false}, {new: true})
			.then(updatedRole => {
				if (updatedRole !== null) {
					return res.send(`${email} authorization level was changed to User: ${JSON.stringify(updatedRole)}`);
				} else {
					return res.send(`Admin information cannot be found. Try again.`);
				};
			})
			.catch(err => {
				return res.send(err.message);
			});
		} else {
			return res.send("Please enter the required information.");
		};
	};

	// Deactivates User (User & Admin)
	module.exports.deactivateUser = (req, res) => {
		let id = req.body.userId;

		// conditional statement for the required fields to be filled out.
		if (id !== "") {

			// finds a user by its id and deactivates.
			User.findByIdAndUpdate({_id: id}, {isActive: false}, {new: true})
			.then(deactivatedUser => {
				if (deactivatedUser !== null) {
					return res.send(`Deactivated User Details: ${JSON.stringify(deactivatedUser)}`);
				} else {
					return res.send(`User information cannot be found. Try again.`)
				};
			})
			.catch(err => {
				return res.send(`Incorrect user Id. Try again.`);
			});			
		} else {
			return res.send("Please enter the required information.")
		};
	};

	// Activates User (User & Admin)
	module.exports.activateUser = (req, res) => {
		let id = req.body.userId;

		// conditional statement for the required fields to be filled out.
		if (id !== "") {

			// finds a user by its id and activates.
			User.findByIdAndUpdate({_id: id}, {isActive: true}, {new: true})
			.then(activatedUser => {
				if (activatedUser !== null) {
					return res.send(`Activated User Details: ${JSON.stringify(activatedUser)}`);
				} else {
					return res.send(`User information cannot be found. Try again.`)
				};
			})
			.catch(err => {
				return res.send(`Incorrect user Id. Try again.`);
			});			
		} else {
			return res.send("Please enter the required information.");
		};
	};

// [DELETE] FUNCTIONALITIES

	// Deletes User (User & Admin)
	module.exports.deleteUser = (req, res) => {
		let id = req.body.userId;

		// conditional statement for the required fields to be filled out.
		if (id !== "") {

			// finds a user by its id and deletes.
			User.findByIdAndDelete({_id: id})
			.then(deletedUser => {
				if (deletedUser !== null) {
					return res.send(`Deleted User Details: ${JSON.stringify(deletedUser)}`);
				} else {
					return res.send(`User information cannot be found. Try again.`);
				};
			})
			.catch(err => {
				return res.send(`Incorrect user Id. Try again.`);				
			});
		} else {
			return res.send("Please enter the required information.");
		};
	};

	// Deletes Admin (Admin)
	module.exports.deleteAdmin = (req, res) => {
		let id = req.body.adminId;

		// conditional statement for the required fields to be filled out.
		if (id !== "") {

			// finds a user by its id and deletes.
			User.findByIdAndDelete({_id: id})
			.then(deletedAdmin => {
				if (deletedAdmin !== null) {
					return res.send(`Deleted Admin Details: ${JSON.stringify(deletedAdmin)}`);
				} else {
					return res.send(`Admin information cannot be found. Try again.`);
				};
			})
			.catch(err => {
				return res.send(`Incorrect admin Id. Try again.`);				
			});
		} else {
			return res.send("Please enter the required information.");
		};		
	};

