// DEPENDENCIES AND MODULES
	const Order = require("../models/Order");
	const User = require("../models/User");
	const Product = require("../models/Product");

// [CREATE] FUNCTIONALITIES

	// Creates An Order (User & Admin)
	module.exports.createOrder = async (req, res) => {
		let email = req.body.orderedBy;
		let productName = req.body.productName;
		let quantity = req.body.quantity;

		// Checks for active products and returns the found price
		let price = await Product.findOne({name: productName, isActive: true})
		.then(foundProduct => {
			return foundProduct.price;
		})
		.catch(err => {
			return false;
		});

		// Checks for the availability of stocks of the product
		let isStocksAvailable = await Product.findOne({name: productName, isActive: true})
		.then(foundProduct => {
			if (foundProduct.stocks >= quantity) {
				return true;
			} else {
				return false;
			}
		})
		.catch(err => {
			return false;
		});

		// conditional statement for the availability of stocks.
		if (isStocksAvailable === false) {
			return res.send("Product cannot be ordered because of inavailability of stocks. Try again at a later time.")
		} else {
			// conditional statement for retrieved data for price.
			if (price === false) {
				return res.send("Product cannot be ordered due to inactive status. Try again at a later time.");
			} else {

				// computes the totalAmount
				let totalAmount = price*quantity;

				// conditional statement for the required fields to be filled out.
				if (email !== "" && productName !== "" && quantity !== "") {

					// returns a Boolean if the order is found.
					let isOrderUpdated = await Order.find({orderedBy: email, productName: productName, quantity: quantity})
					.then(foundOrder => {

						// creates a new instance of order
						let newOrder = new Order({
							totalAmount: totalAmount,
							orderedBy: email,
							productName: productName,
							price: price,
							quantity: quantity
						});

						// saves a new order.
						return newOrder.save()
						.then(savedOrder => true)
						.catch(err => {
							return res.send(err.message);
						});
					})
					.catch(err => {
						return res.send(err.message);
					});

					if (isOrderUpdated !== true) {
						return isOrderUpdated;
					};

					// returns a Boolean if the user is found.
					let isUserUpdated = await User.findOne({email: email})
					.then(foundUser => {
						let orderDetails = {
							productName: productName,
							price: price,
							quantity: quantity
						};

						foundUser.ordersHistory.push(orderDetails);

						// saves the updated user.
						return foundUser.save()
						.then(savedUser => true)
						.catch(err => {
							return res.send(err.message);
						});
					})
					.catch(err => {
						return res.send(err.message);
					});

					if (isUserUpdated !== true) {
						return isUserUpdated;
					};

					// if both await functions return true, it updates the number of stocks and saves the updated product inventory.
					if (isUserUpdated && isOrderUpdated) {

						// finds a product by its name.
						Product.findOne({name: productName})
						.then(foundProduct => {

							foundProduct.stocks -= quantity;

							// saves the updated product.
							return foundProduct.save()
							.then(savedProduct => true)
							.catch(err => {
								return res.send(err.message);
							});
						})
						.catch(err => {
							return res.send(err.message);
						});
						return res.send(`You ordered successfully. Thank you.`);
					};
				} else {
					return res.send("Please enter all the required information.")
				};
			};
		};
	};

	// Checkout Order (User & Admin)
	module.exports.checkoutOrder = (req, res) => {
		let email = req.body.email;
		let orderId = req.body.orderId;
		let paymentMethod = req.body.paymentMethod;
		let shippingAddress = req.body.shippingAddress;

		// conditional statement for the required fields to be filled out.
		if (orderId !== "" && paymentMethod !== "" && shippingAddress !== "") {

			// finds an order by its id.
			Order.findById({_id: orderId})
			.then(foundOrder => {
				if (foundOrder !== null) {

					// creates an object which will be pushed into the User checkout history.
					let newCheckout = {
						totalAmount: foundOrder.totalAmount,
						productName: foundOrder.productName,
						quantity: foundOrder.quantity,
						paymentMethod: paymentMethod,
						shippingAddress: shippingAddress
					};

					// finds the user by its email, in order to push the user checkout.
					User.findOne({email: email})
					.then(foundUser => {
						if (foundUser !== null) {

							foundUser.checkoutHistory.push(newCheckout);

							// saves the updated user.
							return foundUser.save()
							.then(savedUser => {
								return res.send("Checkout Transaction was Successful! Thank you for choosing us.")
							})
							.catch(err => {
									return res.send(err.message);
							});
								
						} else {
							return res.send("Checkout Transaction Error! Please provided the correct details.")
						};
					})
					.catch(err => {
						return res.send(err.message);
					});						
				} else {
					return res.send("Order Details cannot be found. Make sure you provided the correct orderId.")
				};
			})
			.catch(err => {
				return res.send(err.message);
			});
		} else {
			return res.send("Please enter all the required information.");
		};
	};

// [RETRIEVE] FUNCTIONALITIES

	// Retrieves All Orders (Admin)
	module.exports.retrieveAllOrders = (req, res) => {
		Order.find({})
		.then(foundOrders => {
			return res.send(foundOrders);
		})
		.catch(err => {
			return res.send(err.message);
		});
	};

	//Retrieves A Single Order (User & Admin)
	module.exports.getSingleOrder = (req, res) => {
		let orderId = req.body.orderId;

		// conditional statement for the required fields to be filled out.
		if (orderId !== "") {

			// finds an order by its id.
			Order.findById({_id: orderId})
			.then(foundOrder => {
				return res.send(foundOrder);
			})
			.catch(err => {
				return res.send(err.message);
			});	
		} else {
			return res.send("Please enter the required information.");
		};
	};

// [UPDATE] FUNCTIONALITIES


// [DELETE] FUNCTIONALITIES